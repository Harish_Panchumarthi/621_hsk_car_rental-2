import {Tag, Video} from '@uab.lakeshore.collaborative/next-base';

export class VideosByTags {
  public tag:Tag;
  public videos:Video[];

  constructor() {

  }
}
