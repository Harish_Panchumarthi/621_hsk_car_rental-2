import {User} from '@uab.lakeshore.collaborative/next-base';

export class LoginReportModel {
  public user:User = new User();
  public logins_per_week:number[] = [];

  constructor() {

  }
}
