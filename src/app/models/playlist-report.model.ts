import {User} from '@uab.lakeshore.collaborative/next-base';
import {PlaylistReportWeekModel} from './playlist-report-week.model';

export class PlaylistReportModel {
  public user:User;
  public week_1:PlaylistReportWeekModel;
  public week_2:PlaylistReportWeekModel;
  public week_3:PlaylistReportWeekModel;
  public week_4:PlaylistReportWeekModel;
  public week_5:PlaylistReportWeekModel;
  public week_6:PlaylistReportWeekModel;
  public week_7:PlaylistReportWeekModel;
  public week_8:PlaylistReportWeekModel;
  public week_9:PlaylistReportWeekModel;
  public week_10:PlaylistReportWeekModel;
  public week_11:PlaylistReportWeekModel;
  public week_12:PlaylistReportWeekModel;
  public week_13:PlaylistReportWeekModel;
  public week_14:PlaylistReportWeekModel;
  public week_15:PlaylistReportWeekModel;
  public week_16:PlaylistReportWeekModel;

  constructor() {

  }
}
