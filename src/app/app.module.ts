import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AboutUsComponent} from './pages/about-us/about-us.component';
import { HeaderComponent } from './header/header.component';
import {LocationsComponent} from './pages/locations/locations.component';
import {BookCarComponent} from './pages/book-car/book-car.component';
import {AdminSignUpComponent} from './pages/admin-sign-up/admin-sign-up.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminSignUpComponent,
    AboutUsComponent,
    HeaderComponent,
    BookCarComponent,
    LocationsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
