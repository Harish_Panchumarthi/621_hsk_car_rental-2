from flask import Flask, render_template, request, redirect, flash, url_for
import sqlite3
import datetime
import time
import os

app=Flask(__name__)

@app.route('/')
def home():
  return render_template('home.html')

@app.route('/clogin')
def clogin():
  return render_template('customer_login.html')

@app.route('/alogin')
def alogin():
  return render_template('admin_login.html')

@app.route('/custsignup')
def custsignup():
  return render_template('customersignup.html')

@app.route('/adminsignup')
def adminsignup():
  return render_template('adminsignup.html')

@app.route('/custlogin',methods = ['POST', 'GET'])
def custlogin():
  if request.method == 'POST':

    con = sqlite3.connect('mydatabase.db')
    uname = request.form['uname']
    pwd = request.form['pwd']
    cursorObj = con.cursor()
    cur = con.cursor()
    cursorObj = con.cursor()
    login=cursorObj.execute("SELECT * FROM customer WHERE uname= ? and pwd= ?;",(uname,pwd))
    con.commit()
    if (len(login.fetchall()) > 0):
      return render_template("custindex.html")
    else:
      return render_template("custlogin.html")

@app.route('/adminlogin',methods = ['POST', 'GET'])
def adminlogin():
  if request.method == 'POST':


    con = sqlite3.connect('mydatabase.db')
    uname = request.form['uname']
    pwd = request.form['pwd']
    cursorObj = con.cursor()
    cur = con.cursor()
    cursorObj = con.cursor()
    login=cursorObj.execute("SELECT * FROM admin WHERE uname= ? and pwd= ?;",(uname,pwd))
    con.commit()
    if (len(login.fetchall()) > 0):
      return render_template("home3.html")
    else:
      return render_template("adminlogin.html")


@app.route('/custreg',methods = ['POST', 'GET'])
def custreg():
  if request.method == 'POST':


    con = sqlite3.connect('mydatabase.db')
    fname = request.form['fname']
    lname = request.form['lname']
    gender = request.form['gender']
    age  = request.form['age']
    email = request.form['email']
    phone  = request.form['phone']
    pwd  = request.form['pwd']
    rpwd  = request.form['rpwd']
    address=request.form['address']
    cursorObj = con.cursor()
    cur = con.cursor()

    cursorObj.execute("INSERT INTO customer (fname,lname,uname,gender,age,email,phone,pwd,address) VALUES('"+fname+"', '"+lname+"','"+uname+"', '"+gender+"','"+age+"','"+email+"','"+phone+"','"+pwd+"','"+address+"')")

    con.commit()
    return render_template("thankyou.html")



@app.route('/adminreg',methods = ['POST', 'GET'])
def adminreg():
  if request.method == 'POST':


    con = sqlite3.connect('mydatabase.db')
    fname = request.form['fname']
    lname = request.form['lname']
    uname = request.form['uname']
    gender = request.form['gender']
    age  = request.form['age']
    email = request.form['email']
    phone  = request.form['phone']
    pwd  = request.form['pwd']
    rpwd  = request.form['rpwd']
    address=request.form['address']
    cursorObj = con.cursor()
    cur = con.cursor()
    cursorObj.execute("INSERT INTO admin (fname,lname,uname,gender,age,email,phone,pwd,address) VALUES('"+fname+"', '"+lname+"','"+uname+"', '"+gender+"','"+age+"','"+email+"','"+phone+"','"+pwd+"','"+address+"')")
    con.commit()
    return render_template("thankyou.html")

@app.route('/listadmin')
def listadmin():
  con = sqlite3.connect("mydatabase.db")
  con.row_factory = sqlite3.Row

  cur = con.cursor()
  cur.execute("select * from admin")

  rows = cur.fetchall();
  print(rows);
  return render_template("details1.html",rows = rows)
@app.route('/listcustomer')
def listcustomer():
  con = sqlite3.connect("mydatabase.db")
  con.row_factory = sqlite3.Row

  cur = con.cursor()
  cur.execute("select * from customer")

  rows = cur.fetchall();

  return render_template("details.html",rows = rows)
@app.route('/deletecustomer',methods = ['POST', 'GET'])
def deletecustomer():
  if request.method == 'POST':

    con = sqlite3.connect('mydatabase.db')
    id  = request.form['id']
    cursorObj = con.cursor()
    cur = con.cursor()
    cursorObj.execute("DELETE FROM customer where id="+id+"")
    con.commit()
  return render_template("home2.html")
@app.route('/deleteadmin',methods = ['POST', 'GET'])
def deleteadmin():
  if request.method == 'POST':

    con = sqlite3.connect('mydatabase.db')
    id  = request.form['id']
    cursorObj = con.cursor()
    cur = con.cursor()
    cursorObj.execute("DELETE FROM admin where id="+id+"")
    con.commit()
  return render_template("home3.html")
@app.route('/resetcustpwd',methods = ['POST', 'GET'])
def resetcustpwd():
  if request.method == 'POST':


    con = sqlite3.connect('mydatabase.db')
    newpwd = request.form['pwd']
    id  = request.form['id']
    cursorObj = con.cursor()
    cur = con.cursor()
    cursorObj.execute('UPDATE customer SET pwd = ? where id =?;',(newpwd,id))
    con.commit()

  return render_template("index.html")
@app.route('/resetadminpwd',methods = ['POST', 'GET'])
def resetadminpwd():
  if request.method == 'POST':


    con = sqlite3.connect('mydatabase.db')
    newpwd = request.form['pwd']
    id  = request.form['id']
    cursorObj = con.cursor()
    cur = con.cursor()
    cursorObj.execute('UPDATE admin SET pwd = ? where id =?;',(newpwd,id))
    con.commit()

  return render_template("index.html")

# sandeep
@app.route('/listcars')
def listcars():
  con = sqlite3.connect('carrental.db')
  cursor = con.cursor()
  cursor.execute("SELECT * FROM vehicle")
  customerlist = cursor.fetchall()
  cursor.close()
  print(customerlist)
  return render_template('home.html', records=customerlist)

@app.route('/findcar',methods=['GET','POST'])
def findcar():
  con = sqlite3.connect('carrental.db')
  cursor = con.cursor()
  pickdate = request.form["start_date"]
  dropdate = request.form["end_date"]
  location = request.form["location"]
  cursor.execute("SELECT * FROM vehicle WHERE location = '"+location+"'")
  carlistbylocation = cursor.fetchall()
  cursor.close()
  return render_template('bookcar.html', records=carlistbylocation,start_date=pickdate,end_date=dropdate)


@app.route("/addcarform",methods=['GET','POST'])

def addcarform():
  con = sqlite3.connect('carrental.db')
  cursor = con.cursor()
  model = request.form["model"]
  rno = request.form["rno"]
  seating = request.form["seating"]
  vtype = request.form["vtype"]
  priceperday = request.form["priceperday"]
  location = request.form["location"]
  vstatus = "available"

  cursor.execute("INSERT INTO vehicle (model,rno,seating,vtype,priceperday,vstatus,location) VALUES ('"+model+"', '"+rno+"', '"+seating+"','"+vtype+"','"+priceperday+"','"+vstatus+"','"+location+"')")
  con.commit()
  cursor.close()
  #flash("Adding New Car is successfull !!!")
  return redirect(url_for('home'))


@app.route('/delcar/<vid>', methods=['GET','POST'])
def delrec(vid):
  con = sqlite3.connect('carrental.db')
  cursor = con.cursor()
  # vid = str(request.form["vid"])
  cursor.execute("DELETE FROM vehicle WHERE vid = '"+vid+"'")
  con.commit()
  cursor.close()
  # flash("Car Successfully Deleted !!!")
  return redirect(url_for('home'))

@app.route('/updaterec', methods=['GET','POST'])
def updaterec():
  con = sqlite3.connect('carrental.db')
  cursor = con.cursor()
  vid = request.form["vid"]
  model = request.form["model"]
  rno = request.form["rno"]
  seating = request.form["seating"]
  vtype = request.form["vtype"]
  priceperday = request.form["priceperday"]
  location = request.form["location"]
  vstatus = request.form["vstatus"]

  cursor.execute("UPDATE vehicle SET model='"+model+"',rno='"+rno+"',seating='"+seating+"',vtype='"+vtype+"',priceperday='"+priceperday+"',vstatus='"+vstatus+"',location='"+location+"' WHERE vid='"+vid+"'")
  con.commit()
  cursor.close()
  #flash("Car updated successfully")
  return redirect(url_for('home'))

if __name__=='__main__':
  app.run(debug=True)
