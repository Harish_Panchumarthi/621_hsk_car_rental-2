from flask import Flask, render_template, request, redirect, url_for, flash
import sqlite3
import datetime
import time
import os

app=Flask(__name__)
con = sqlite3.connect('mydatabase.db')
cursor = con.cursor()

# variables initialization
cust_id = ""
driver_id = 0
car_id = ""
payment_type = ""
payment_status = ["Paid","Not Paid"]
booking_id = 0

@app.route('/listcars')
def home():
    con = sqlite3.connect('carrental.db')
    cursor = con.cursor()
    cursor.execute("SELECT * FROM vehicle")
    customerlist = cursor.fetchall()
    cursor.close()
    print(customerlist)
    return render_template('home.html', records=customerlist)

@app.route('/findcar',methods=['GET','POST'])
def findcar():
    con = sqlite3.connect('carrental.db')
    cursor = con.cursor()
    pickdate = request.form["start_date"]
    dropdate = request.form["end_date"]
    location = request.form["location"]
    cursor.execute("SELECT * FROM vehicle WHERE location = '"+location+"'")
    carlistbylocation = cursor.fetchall()
    cursor.close()
    return render_template('bookcar.html', records=carlistbylocation,start_date=pickdate,end_date=dropdate)


@app.route("/addcarform",methods=['GET','POST'])

def addcarform():
    con = sqlite3.connect('carrental.db')
    cursor = con.cursor()
    model = request.form["model"]
    rno = request.form["rno"]
    seating = request.form["seating"]
    vtype = request.form["vtype"]
    priceperday = request.form["priceperday"]
    location = request.form["location"]
    vstatus = "available"

    cursor.execute("INSERT INTO vehicle (model,rno,seating,vtype,priceperday,vstatus,location) VALUES ('"+model+"', '"+rno+"', '"+seating+"','"+vtype+"','"+priceperday+"','"+vstatus+"','"+location+"')")
    con.commit()
    cursor.close()
    #flash("Adding New Car is successfull !!!")
    return redirect(url_for('home'))


@app.route('/delcar/<vid>', methods=['GET','POST'])
def delrec(vid):
    con = sqlite3.connect('carrental.db')
    cursor = con.cursor()
    # vid = str(request.form["vid"])
    cursor.execute("DELETE FROM vehicle WHERE vid = '"+vid+"'")
    con.commit()
    cursor.close()
    # flash("Car Successfully Deleted !!!")
    return redirect(url_for('home'))

@app.route('/updaterec', methods=['GET','POST'])
def updaterec():
    con = sqlite3.connect('carrental.db')
    cursor = con.cursor()
    vid = request.form["vid"]
    model = request.form["model"]
    rno = request.form["rno"]
    seating = request.form["seating"]
    vtype = request.form["vtype"]
    priceperday = request.form["priceperday"]
    location = request.form["location"]
    vstatus = request.form["vstatus"]

    cursor.execute("UPDATE vehicle SET model='"+model+"',rno='"+rno+"',seating='"+seating+"',vtype='"+vtype+"',priceperday='"+priceperday+"',vstatus='"+vstatus+"',location='"+location+"' WHERE vid='"+vid+"'")
    con.commit()
    cursor.close()
    #flash("Car updated successfully")
    return redirect(url_for('home'))

if __name__ == '__main__':
    #app.run()
    app.run(debug=True)
